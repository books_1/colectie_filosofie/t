# T

## Content

```
./Tadeusz Kotarbinski:
Tadeusz Kotarbinski - Tratat despre lucrul bine facut.pdf

./Ted Honderich:
Ted Honderich - Cat de liber esti.pdf

./Teodor Baconski:
Teodor Baconski - Facebook, fabrica de narcisism.pdf

./Teodor Dima:
Teodor Dima - Metodele inductive.pdf

./Teodor Stihi:
Teodor Stihi - Introducere in logica simbolica.pdf

./Thierry Gonthier:
Thierry Gonthier - Marile opere ale filosofiei moderne.pdf

./Thoma de Aquino:
Thoma de Aquino - Despre Fiinta si Esenta.pdf

./Thomas A. Szlezak:
Thomas A. Szlezak - Cum sa-l citim pe Platon.pdf

./Thomas Hobbes:
Thomas Hobbes - Elementele dreptului natural si politic.pdf

./Thomas Kuhn:
Thomas Kuhn - Structura revolutiilor stiintifice.pdf
Thomas Kuhn - Tensiunea esentiala.pdf

./Thomas Laqueur:
Thomas Laqueur - Corpul si sexul.pdf

./Thomas Mann:
Thomas Mann - Scrisori.pdf

./Thomas Nagel:
Thomas Nagel - Oare ce inseamna toate astea.pdf
Thomas Nagel - Perspectiva de nicaieri.pdf
Thomas Nagel - Ultimul cuvant.pdf
Thomas Nagel - Vesnice intrebari.pdf

./Toma de Aquino:
Toma de Aquino - Despre fiind si esenta.pdf
Toma de Aquino - Despre guvernamant.pdf
Toma de Aquino - Despre principiile naturii.pdf
Toma de Aquino - Despre unitatea intelectului contra averroistilor.pdf
Toma de Aquino - Summa Theologiae vol. 1.pdf

./Tommaso Campanella:
Tommaso Campanella - Cetatea soarelui (Poezii filosofice).pdf

./Tom Sorell:
Tom Sorell - Descartes, o scurta introducere.pdf

./Tudor Arghezi:
Tudor Arghezi - Eminescu.pdf
```

